# Tic Tac Toe
import os
os.system('clear')

restart = 0
turn = 0
player = 'X'
tile_bag = [1,2,3,4,5,6,7,8,9]
choices = [1,2,3,4,5,6,7,8,9, 'Restart', 'Exit']

def print_board():
    os.system('clear')
    num = -1
    print('Tic-Tac-Toe\n')
    for i in range(3):
        num += 3
        print(tile_bag[num-2],tile_bag[num-1],tile_bag[num])
def win():
    print_board()
    print('\nPlayer \''+player+'\' Wins')
    input('Press \'Enter\' To End ')
    os.system('clear')
    exit()

try:
    while True:
        print_board()
        if turn < 9:
            try:
                print('\nChoices:')
                print(choices)
                print('\nPlayer \''+player+'\' Your Turn')
                tile = input('input: ')
                try:
                    if tile[0] == 'E' or tile[0] == 'e':
                        os.system('clear')
                        exit()
                    if tile[0] == 'R' or tile[0] == 'r':
                        os.system('clear')
                        restart = 1
                except IndexError:
                    pass
                if restart == 0:
                    tile = int(tile)
                    if tile_bag.__contains__(tile):
                        tile_bag[tile-1] = player
                        choices.remove(tile)
                        turn += 1
                        num = -1
                        for i in range(3):
                            num += 3
                            if tile_bag[num] == player:
                                if tile_bag[num-1] == player:
                                    if tile_bag[num-2] == player:
                                        win()
                        num = -1
                        for i in range(3):
                            num += 1
                            if tile_bag[num] == player:
                                if tile_bag[num+3] == player:
                                    if tile_bag[num+6] == player:
                                        win()
                        if tile_bag[0] == player: 
                            if tile_bag[4] == player:
                                if tile_bag[8] == player:
                                    win()
                        if tile_bag[2] == player:
                            if tile_bag[4] == player:
                                if tile_bag[6] == player:
                                    win()
                        if player == 'X':
                            player = '□'
                        else:
                            player = 'X'
                    elif tile < 1 or tile > 9: 
                        print('\nMust Be A Number Between 1 And 9')
                        input('Press \'Enter\' To Continue ')
                    else:
                        print('\nNumber Already Used')
                        input('Press \'Enter\' To Continue ')
                    os.system('clear')
                else:
                    restart = 0
                    tile = 0
                    turn = 0
                    player = 'X'
                    tile_bag = [1,2,3,4,5,6,7,8,9]
                    choices = [1,2,3,4,5,6,7,8,9, 'Restart', 'Exit']
                    print_board()
            except ValueError:
                print('\nMust Be A Number, \'Exit\' or \'Restart\'')   
                input('Press \'Enter\' To Continue ')
                os.system('clear')
        else:
            print('\nEnd Of Game, Tie')
            input('Press \'Enter\' To End ')
            os.system('clear')
            exit()
except EOFError:
    os.system('clear')