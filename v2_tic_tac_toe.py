# Tic Tac Toe
import os
os.system('clear')

turn = 0
player = 'X'
tile_bag = [1,2,3,4,5,6,7,8,9]

try:
    while True:
        os.system('clear')
        print('\'Ctrl + D\' To Leave')
        num = -1
        for i in range(3):
            num += 3
            print(tile_bag[num-2],tile_bag[num-1],tile_bag[num])
        if turn < 9:
            try:
                print('Choose A Tile Player \''+player+'\' ')
                tile = int(input(''))
                if tile_bag.__contains__(tile):
                    tile_bag[tile-1] = player
                    turn += 1
                    if player == 'X':
                        player = '□'
                    else:
                        player = 'X'
                elif tile < 1 or tile > 9: 
                    print('Must Be A Number Between 1 And 9')
                    input('Press \'Enter\' To Continue ')
                else:
                    print('Number Already Used')
                    input('Press \'Enter\' To Continue ')
                os.system('clear')
            except ValueError:
                print('Must Be A Number')
                input('Press \'Enter\' To Continue ')
                os.system('clear')
        else:
            print('End Of Game, Out Of Moves')
            input('Press \'Enter\' To End ')
            os.system('clear')
            exit()
except EOFError:
    os.system('clear')
    print('Goodbye')
