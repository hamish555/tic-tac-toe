# Tic Tac Toe
import os

os.system('clear')

player = "X"

t1 = "1"
t2 = "2"
t3 = "3"
t4 = "4"
t5 = "5"
t6 = "6"
t7 = "7"
t8 = "8"
t9 = "9"

try:
    input('Welcome to Tic Tac Toe.\n\'X\' Starts.\nPress \'Enter\' to Start ')
    os.system('clear')

    while True:
        while True:
            print('[', t1, ',', t2, ',', t3, ']\n[', t4, ',', t5, ',', t6, ']\n[', t7, ',', t8, ',', t9, ']')
            try:
                print('Choose A Tile Player \''+player+'\' ')
                tile = int(input(''))
            except ValueError:
                print('Must Be A Number')
                input('Press \'Enter\' To Continue ')
                os.system('clear')
            else:
                os.system('clear')
            if tile == 1 and t1 == "1":
                t1 = player
                if player == "X":
                    player = "O"
                else:
                    player = "X"
            elif tile == 2 and t2 == "2":
                t2 = player
                if player == "X":
                    player = "O"
                else:
                    player = "X"
            elif tile == 3 and t3 == "3":
                t3 = player
                if player == "X":
                    player = "O"
                else:
                    player = "X"
            elif tile == 4 and t4 == "4":
                t4 = player
                if player == "X":
                    player = "O"
                else:
                    player = "X"
            elif tile == 5 and t5 == "5":
                t5 = player
                if player == "X":
                    player = "O"
                else:
                    player = "X"
            elif tile == 6 and t6 == "6":
                t6 = player
                if player == "X":
                    player = "O"
                else:
                    player = "X"
            elif tile == 7 and t7 == "7":
                t7 = player
                if player == "X":
                    player = "O"
                else:
                    player = "X"
            elif tile == 8 and t8 == "8":
                t8 = player
                if player == "X":
                    player = "O"
                else:
                    player = "X"
            elif tile == 9 and t9 == "9":
                t9 = player
                if player == "X":
                    player = "O"
                else:
                    player = "X"
            else:
                input('Invalid Tile\nPress \'Enter\' To Continue ')
                os.system('clear')
except EOFError:
    os.system('clear')
    print('Goodbye')