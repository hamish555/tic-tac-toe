# Tic Tac Toe
import os
os.system('clear')

turn = 0
player = 'X'
tile_bag = [1,2,3,4,5,6,7,8,9]
choices = [1,2,3,4,5,6,7,8,9, 'Exit']

try:
    while True:
        os.system('clear')
        num = -1
        print('Tic-Tac-Toe\n')
        for i in range(3):
            num += 3
            print(tile_bag[num-2],tile_bag[num-1],tile_bag[num])
        if turn < 9:
            try:
                print('\nChoices:')
                print(choices)
                print('\nPlayer \''+player+'\' Your Turn')
                tile = input('input: ')
                if tile[0] == 'E' or tile[0] == 'e':
                    os.system('clear')
                    exit()
                else:
                    pass
                tile = int(tile)
                if tile_bag.__contains__(tile):
                    tile_bag[tile-1] = player
                    choices.remove(tile)
                    turn += 1
                    if player == 'X':
                        player = '□'
                    else:
                        player = 'X'
                elif tile < 1 or tile > 9: 
                    print('\nMust Be A Number Between 1 And 9')
                    input('Press \'Enter\' To Continue ')
                else:
                    print('\nNumber Already Used')
                    input('Press \'Enter\' To Continue ')
                os.system('clear')
            except ValueError:
                print('\nMust Be A Number or \'Exit\'')
                input('Press \'Enter\' To Continue ')
                os.system('clear')
        else:
            print('\nEnd Of Game, Out Of Moves')
            input('Press \'Enter\' To End ')
            os.system('clear')
            exit()
except EOFError:
    os.system('clear')